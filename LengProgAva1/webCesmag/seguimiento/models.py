from django.db import models


# Create your models here.
class UserQuery(models.Model):

    name = models.CharField(max_length=150, verbose_name='Nombre')
    telephone = models.CharField(max_length=150, verbose_name='Telefono')
    mail = models.EmailField(max_length=254, verbose_name='Correo electronico', blank=True)
    program = models.CharField(max_length=150, verbose_name='Programa interes')
    question = models.TextField(verbose_name='Pregunta')
    status = models.BooleanField(verbose_name='¿Atendido?')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Editado')

    class Meta:
        verbose_name = 'Consulta'
        verbose_name_plural = 'Consultas'
        ordering = ['-created_at']

    def __str__(self):
        return self.name
