from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from django.core.mail import EmailMessage, EmailMultiAlternatives
from .models import UserQuery
from django.template.loader import get_template
from django.conf import settings
from smtplib import SMTPException

# Create your views here.


# envio correos
def send_email(mail, name, telephone, program, question):

    context = {'mail': mail, 'name': name, 'telephone': telephone, 'program': program, 'question': question}
    template = get_template('correo.html')
    content = template.render(context)
    email = EmailMultiAlternatives(
        'Cordial Saludo',
        'Daniel prueba',
        settings.EMAIL_HOST_USER,
        [mail, settings.EMAIL_HOST_USER]
    )

    email.attach_alternative(content, 'text/html')
    try:
        email.send()
    except SMTPException as e:
        print('There was an error sending an email: ', e)


# Guardar la consulta del aspirante
def saveUserQuery(request):
    # Conprovacion llegada por get##
    if request.method == 'POST':

        name = request.POST.get('nombres')
        telephone = request.POST.get('telefono')
        mail = request.POST.get('correo')
        program = request.POST.get('programa')
        question = request.POST.get('consulta')
        status = False
        userQuery = UserQuery(
            name=name,
            telephone=telephone,
            mail=mail,
            program=program,
            question=question,
            status=status
        )
        userQuery.save()
        send_email(mail, name, telephone, program, question)

        messages.success(request, 'Pregunta Registrada Correctamente')
        return redirect('index')
    else:
        messages.success(request, 'lo sentimos tu consulta no se a podido almacenar')
        return redirect('index')
