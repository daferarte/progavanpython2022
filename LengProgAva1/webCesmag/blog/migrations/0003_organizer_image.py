# Generated by Django 3.1.2 on 2020-11-07 04:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_auto_20201106_0316'),
    ]

    operations = [
        migrations.AddField(
            model_name='organizer',
            name='image',
            field=models.ImageField(default='null', upload_to='articles', verbose_name='Imagen'),
        ),
    ]
