from django.contrib import admin
from .models import LocationSite, Content


# clase LocationSiteAdmin sirve para ordenar la lista
class LocationSiteAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )
    search_fields = ('name', 'description')
    # list_filter = ('visible',)
    list_display = ('name', 'created_at')
    ordering = ('-created_at', )


# clase ContentAdmin sirve para ordenar la lista
class ContentAdmin(admin.ModelAdmin):
    readonly_fields = ('created_at', )
    search_fields = ('title', 'contentizq')
    # list_filter = ('visible',)
    list_display = ('title', 'created_at')
    ordering = ('-created_at', )

    def save_model(self, request, obj, form, change):
        if not obj.user_id:
            obj.user_id = request.user.id
        obj.save()


# Register your models here.
admin.site.register(LocationSite, LocationSiteAdmin)
admin.site.register(Content, ContentAdmin)
