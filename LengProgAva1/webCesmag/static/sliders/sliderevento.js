var timer = 4000;

var i = 0;
var max = $('#c > li').length;
    
	$("#c > li").eq(i).addClass('activesli').css('opacity','1','left','0');
	$("#c > li").eq(i + 1).addClass('activesli').css('opacity','1','left','33%');
	$("#c > li").eq(i + 2).addClass('activesli').css('opacity','1','left','63%');
 

	setInterval(function(){ 

		$("#c > li").removeClass('activesli');

		$("#c > li").eq(i).css('opacity','0','transition-delay','0.25s');
		$("#c > li").eq(i + 1).css('opacity','0','transition-delay','0.5s');
		$("#c > li").eq(i + 2).css('opacity','0','transition-delay','0.75s');

		if (i < max-3) {
			i = i+3; 
		}

		else { 
			i = 0; 
		}  

		$("#c > li").eq(i).css('left','0').addClass('activesli').css('opacity','1','transition-delay','1.25s');
		$("#c > li").eq(i + 1).css('left','33%').addClass('activesli').css('opacity','1','transition-delay','1.5s');
		$("#c > li").eq(i + 2).css('left','63%').addClass('activesli').css('opacity','1','transition-delay','1.75s');
	
	}, timer);
 