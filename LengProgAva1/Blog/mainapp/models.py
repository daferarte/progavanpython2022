from django.db import models
from django.contrib.auth.models import User

# Create your models here.

# clase Content maneja los contenidos del index de manera dinamica
class Persona(models.Model):
    cedula = models.CharField(max_length=20, verbose_name='Cedula')
    Nombre = models.CharField(max_length=150, verbose_name='Nombre')
    Apellido = models.CharField(max_length=150, verbose_name='Apellido')
    #contentizq = RichTextField(verbose_name='contenido izquierdo')
    #image = models.ImageField(default='null', verbose_name="Imagen", upload_to="contenido")
    sex = models.BooleanField(verbose_name="Sexo F")
    user = models.ForeignKey(User, editable=False, verbose_name="Usuario", on_delete=models.PROTECT)  # clave relacional del modelo de usuarios de django
    #LocationSite = models.ManyToManyField(LocationSite, verbose_name="Ubicacion", blank=True)  # clave relacional del modelo de organizador de django
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Creado')
    update_at = models.DateTimeField(auto_now=True, verbose_name='Editado')

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        ordering = ['-created_at']

    def __str__(self):
        return self.cedula

    # esto es el final de la linea
# esto es el final de la linea
